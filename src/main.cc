#include <thread>
#include <iostream>
#include <fstream>
#include <sstream>
#include <atomic>
#include <mutex>
#include <memory>
#include <cstdlib>
#include <semaphore.h>

#include "pipeline.hh"

/// cout mutex
std::mutex mutex;

/// IF/ID Latch Example
/// IF produces the instruction to be consumed and decoded by the ID stage
struct IFIDLatch
{
    std::string instruction;
};

class stage
{
    public:
        stage(p::pipeline& p)
            : p_ (p)
        {
        }

    protected:
        p::pipeline& p_;
};

class IFStage : public stage
{
    std::vector<std::string> program_;
    int pc_;
    sem_t terminate_sync_;
    public:
        IFStage(p::pipeline& p, const std::string& file)
            : stage (p), pc_(0)
        {
            sem_init(&terminate_sync_, 0, 1);
            std::string instruction;

            std::ifstream f(file);

            while (std::getline(f, instruction))
            {
                if (f.bad())
                    return;
                program_.push_back(instruction);
            }
        }

        void* operator()(void* sr)
        {
            (void) sr;
            mutex.lock();
            std::cout << "pc: " << pc_ << "\n";
            mutex.unlock();

            if (pc_ == program_.size())
            {
                return p::latch::terminate;
            }

            mutex.lock();
            std::cout << "IF " << p_.t_get() << ": " << program_[pc_] << "\n";
            mutex.unlock();

            auto* latch =  new IFIDLatch
            {
                .instruction = program_[pc_++]
            };
            sem_post(&terminate_sync_);
            return latch;
        }

        void set_pc(int pc)
        {
            sem_wait(&terminate_sync_);
            pc_ = pc;
            mutex.lock();
            std::cout << "pc set to " << pc << "\n";
            mutex.unlock();
        }

        void no_jump()
        {
            sem_wait(&terminate_sync_);
        }
};

struct IDEXlatch
{
    std::string op;
    int out;
    int off1;
    int reg1;
    int in1;
    int reg2;
    int in2;

    IDEXlatch()
    : out(0), off1(0), in1(0), in2(0)
    {}
};

class IDStage : public stage
{
    IFStage& if_;
    std::vector<int> regs_;
    public:
        IDStage(p::pipeline& p, IFStage& ifs)
            : stage (p), regs_(32), if_(ifs)
        {
            for (int i = 0; i < 32; ++i)
                regs_[i] = i;
        }

        void* operator()(void* sr)
        {
            std::unique_ptr<IFIDLatch> in(static_cast<IFIDLatch*>(sr));

            std::unique_ptr<IDEXlatch> out(new IDEXlatch);

            std::istringstream splitter(in->instruction);
            splitter >> out->op;

            if (out->op == "lw")
            {
                splitter >> out->out; // out reg
                splitter >> out->off1; // offset
                splitter >> out->reg1; // register
                out->in1 = regs_[out->reg1];

                mutex.lock();
                std::cout << "ID " << p_.t_get() << ": "
                    << out->op << " r"
                    << out->out << ", "
                    << out->in1
                    << std::endl;
                mutex.unlock();
            }
            else if (out->op == "nop")
            {
                if_.no_jump();
                return p::latch::noop;
            }
            else if (out->op == "j")
            {
                splitter >> out->out; // dst
                if_.set_pc(out->out);
                p_.j_end();

                mutex.lock();
                std::cout << "ID " << p_.t_get() << ": "
                    << out->op << " "
                    << out->out
                    << std::endl;
                mutex.unlock();
                return p::latch::noop;
            }
            else
            {
                splitter >> out->out;
                splitter >> out->reg1;
                splitter >> out->reg2;

                out->in1 = regs_[out->reg1];
                out->in2 = regs_[out->reg2];

                mutex.lock();
                std::cout << "ID " << p_.t_get() << ": "
                    << out->op << " r"
                    << out->out << ", "
                    << out->in1 << ", "
                    << out->in2
                    << std::endl;
                mutex.unlock();

            }
            if_.no_jump();

            return out.release();
        }

        void write_reg(int reg, int val)
        {
            regs_[reg] = val;
        }
};

struct EXMEMlatch
{
    std::string op;
    int out;
    int val;
    int addr;

    EXMEMlatch() : out(0), val(0), addr(0) {}
};

class ForwardUnit
{
    int regs_[32];
    std::mutex sem_sync_;
    sem_t push_;
    sem_t pop_;
    bool ex_;
    bool mem_;

    public:
    ForwardUnit()
        : ex_(true), mem_(true)
    {
        std::unique_lock<std::mutex>(sem_sync_);
        sem_init(&push_, 0, 0);
        sem_init(&pop_, 0, 1);
        for (auto& r : regs_)
            r = -1;
    }

    void require_data()
    {
        std::unique_lock<std::mutex>(sem_sync_);
        sem_wait(&pop_);
        ex_ = false;
        mem_ = false;
    }

    void validate_ex()
    {
        std::unique_lock<std::mutex>(sem_sync_);
        ex_ = true;
        if (mem_)
        {
            sem_wait(&push_);
            sem_post(&pop_);
        }
    }

    void validate_mem()
    {
        std::unique_lock<std::mutex>(sem_sync_);
        mem_ = true;
        if (ex_)
        {
            sem_wait(&push_);
            sem_post(&pop_);
        }
    }

    int get_value(int reg, int def_val)
    {
        return regs_[reg] == -1 ? def_val : regs_[reg];
    }

    void add_value(int r, int v)
    {
        regs_[r] = v;
    }
    void end_require_data()
    {
        sem_post(&push_);
        for (auto& r : regs_)
            r = -1;
    }
};

class EXStage : public stage
{
    ForwardUnit& fu_;
    public:
        EXStage(p::pipeline& p, ForwardUnit& fu)
            : stage (p), fu_(fu)
        {
        }

        void* operator()(void* sr)
        {
            std::unique_ptr<IDEXlatch> in(static_cast<IDEXlatch*>(sr));
            std::unique_ptr<EXMEMlatch> out(new EXMEMlatch);
            out->op = in->op;
            out->out = in->out;
            if (in->op == "lw")
            {
                fu_.require_data();
                in->in1 = fu_.get_value(in->reg1, in->in1) + in->off1;
                out->addr = in->in1;
                fu_.end_require_data();

                mutex.lock();
                std::cout << "EX " << p_.t_get() << ": "
                    << in->op << ": r"
                    << in->out << " = data["
                    << in->in1 << "]"
                    << std::endl;
                mutex.unlock();
            }
            else if (in->op == "j")
            {
                
            }
            else
            {
                fu_.require_data();
                if (in->op == "sub")
                {
                    out->val = fu_.get_value(in->reg1, in->in1) -
                        fu_.get_value(in->reg2, in->in2);
                }
                fu_.end_require_data();
                fu_.add_value(out->out, out->val);
                mutex.lock();
                std::cout << "EX " << p_.t_get() << ": "
                    << in->op << " r"
                    << in->out << " = "
                    << out->val
                    << std::endl;
                mutex.unlock();
                p_.sub_end();
            }
            fu_.validate_ex();

            return out.release();
        }
};

class MEMStage : public stage
{
    ForwardUnit& fu_;
    std::vector<int> data_;
    public:
        MEMStage(p::pipeline& p, ForwardUnit& fu)
            : stage (p), fu_(fu)
        {
            for (int i = 0; i < 258; ++i)
                data_.push_back(i);
        }

        void* operator()(void* sr)
        {
            std::unique_ptr<EXMEMlatch> l(static_cast<EXMEMlatch*>(sr));

            if (l->op == "lw")
            {
                l->val = data_[l->addr];
                fu_.add_value(l->out, l->val);
                p_.lw_end();
            }
            fu_.validate_mem();
            mutex.lock();
            std::cout << "MEM " << p_.t_get() << ": "
                << l->op << " r"
                << l->out << " = "
                << l->val
                << std::endl;
            mutex.unlock();

            return l.release();
        }
};

class WBStage : public stage
{
    IDStage& id_;
    public:
        WBStage(p::pipeline& p, IDStage& id)
            : stage (p), id_(id)
        {
        }

        void* operator()(void* sr)
        {
            std::unique_ptr<EXMEMlatch> l(static_cast<EXMEMlatch*>(sr));

            mutex.lock();
            std::cout << "WB " << p_.t_get() << ": " << l->op << std::endl;
            mutex.unlock();

            id_.write_reg(l->out, l->val);

            return l.release();
        }
};

int main(int argc, char* argv[])
{
    if (argc != 2)
        return 1;

    p::pipeline p;
    ForwardUnit fu;

    IFStage ifs(p, argv[1]);
    IDStage ids(p, ifs);
    EXStage exs(p, std::ref(fu));
    MEMStage mems(p, fu);
    WBStage wbs(p, ids);

    // warning: seems to be bugged when optimized with gcc, the first stage is not
    // ifs but wbs oO
    //p.add_stage(std::ref(ifs));
    //p.add_stage(std::ref(ids));
    //p.add_stage(std::ref(exs));
    //p.add_stage(std::ref(mems));
    //p.add_stage(std::ref(wbs));
    // !warning

    // warning: only working add_stage version at any optimization levels
    p.add_stage(std::ref(ifs))
        .add_stage(std::ref(ids))
        .add_stage(std::ref(exs))
        .add_stage(std::ref(mems))
        .add_stage(std::ref(wbs));
    // !warning

    p.run();

    std::cout
        << std::endl
        << "Total Number of Cycles = " << p.t_get() - 1 << std::endl
        << "Average CPI = " << p.cpi() << std::endl
        << "Average IPC = " << 1.0 / p.cpi() << std::endl;

    return 0;
}
